FROM openjdk:8-alpine
COPY /target/Api-Investimentos-*.jar api-investimentos-jar
CMD ["java","-jar", "api-investimentos-jar"]
