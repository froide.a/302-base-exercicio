$service_template = @(END)
[Unit]
Description=API Investimentos Service

[Service]
ExecStart=/usr/bin/java -jar /home/ubuntu/Api-Investimentos-0.0.1-SNAPSHOT.jar

[Install]
WantedBy=multi-user.target
END

class { "java":
    package => "openjdk-8-jdk",
}

file { "/etc/systemd/system/
meuservico.service":
    ensure => file,
    content => inline_epp($service_template),
}

service { "meuservico":
    ensure => running,
}

include nginx

nginx::resource::server { "meuapp":
    listen_port => 80,
    proxy => "http://localhost:8080",
}

file { "/etc/nginx/sites-enabled/default":
    ensure => absent,
    notify => Service["nginx"],
}

ssh_authorized_key { "jenkins@banana":
    ensure => present,
    user => "ubuntu",
    type => "ssh-rsa",
    key => "AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb",
}
